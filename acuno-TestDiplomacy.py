from io import StringIO
from unittest import main, TestCase
import time

from Diplomacy import diplomacy_solve


class TestDiplomacy(TestCase):
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Austin Hold\nB Houston Move Austin\nC Dallas Support A\nD Waco Move Dallas\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Austin Support B\nB Dallas Support A\nC Houston Move Austin\nD Waco Move Dallas")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Austin Move Dallas\nB Dallas Move Paris\nC Paris Hold\nD Seattle Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Dallas\nB [dead]\nC Paris\nD Seattle\n")

    def test_solve_4(self):
        r = StringIO("A Reno Hold\nB Tokyo Support A\nC Okinawa Support B\nD Austin Move Reno\nE London Move Tokyo")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Tokyo\nC Okinawa\nD [dead]\nE [dead]\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

"""
coverage run    --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1
cat TestDiplomacy.out
....
----------------------------------------------------------------------
Ran 4 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          78      0     50      2    98%   17->16, 53->52
TestDiplomacy.py      27      0      2      1    97%   42->exit
--------------------------------------------------------------
TOTAL                105      0     52      3    98%
"""