#!/usr/bin/env python3


# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, Army

"""
I think to test these functions we need to create armies and then run the armies list through the
tests, I'm going to push this to the page but it will fail
"""


# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        info = diplomacy_read(s)
        self.assertEqual(info[0], "A")
        self.assertEqual(info[1], "Madrid")
        self.assertEqual(info[2], "Hold")

    def test_read_2(self):
        s = "B London Support\n"
        info = diplomacy_read(s)
        self.assertEqual(info[0], "B")
        self.assertEqual(info[1], "London")
        self.assertEqual(info[2], "Support")

    def test_read_3(self):
        s = "C Paris Move London\n"
        info = diplomacy_read(s)
        self.assertEqual(info[0], "C")
        self.assertEqual(info[1], "Paris")
        self.assertEqual(info[2], "Move")
        self.assertEqual(info[3], "London")

    



    # ----
    # eval
    # ----
    def test_eval_1(self):
        armies = [Army(["A", "Madrid", "Hold"])]
        diplomacy_eval(armies)
        self.assertEqual((armies[0].name + " " + armies[0].loc), "A Madrid")

    def test_eval_2(self):
        armies = [Army(["A", "Madrid", "Hold"]), Army(["B", "London", "Support", "C"]), Army(["C", "Paris", "Move", "Madrid"])]
        diplomacy_eval(armies)
        self.assertEqual((armies[0].name + " " + armies[0].loc), "A [dead]")
        self.assertEqual((armies[1].name + " " + armies[1].loc), "B London")
        self.assertEqual((armies[2].name + " " + armies[2].loc), "C Madrid")

    def test_eval_3(self):
        armies = [Army(["A", "Madrid", "Move", "London"]), Army(["B", "London", "Hold"])]
        diplomacy_eval(armies)
        self.assertEqual((armies[0].name + " " + armies[0].loc), "A [dead]")
        self.assertEqual((armies[1].name + " " + armies[1].loc), "B [dead]")

    def test_eval_4(self):
        armies = [Army(["A", "Madrid", "Support", "B"]), Army(["B", "London", "Hold"])]
        diplomacy_eval(armies)
        self.assertEqual((armies[0].name + " " + armies[0].loc), "A Madrid")
        self.assertEqual((armies[1].name + " " + armies[1].loc), "B London")

    def test_eval_5(self):
        armies = [Army(["A", "Madrid", "Support", "B"]), Army(["B", "London", "Hold"]), Army(["C", "Paris", "Move", "London"])]
        diplomacy_eval(armies)
        self.assertEqual((armies[0].name + " " + armies[0].loc), "A Madrid")
        self.assertEqual((armies[1].name + " " + armies[1].loc), "B London")
        self.assertEqual((armies[2].name + " " + armies[2].loc), "C [dead]")

    def test_eval_6(self):
        armies = [Army(["A", "Madrid", "Move", "London"]), Army(["B", "London", "Hold"]), Army(["C", "Paris", "Move", "Madrid"])]
        diplomacy_eval(armies)
        self.assertEqual((armies[0].name + " " + armies[0].loc), "A [dead]")
        self.assertEqual((armies[1].name + " " + armies[1].loc), "B [dead]")
        self.assertEqual((armies[2].name + " " + armies[2].loc), "C Madrid")

    def test_eval_7(self):
        armies = [Army(["A", "Madrid", "Move", "London"]), Army(["B", "London", "Hold"]), Army(["C", "Paris", "Support", "A"]), Army(["D", "Prague", "Support", "B"])]
        diplomacy_eval(armies)
        self.assertEqual((armies[0].name + " " + armies[0].loc), "A [dead]")
        self.assertEqual((armies[1].name + " " + armies[1].loc), "B [dead]")
        self.assertEqual((armies[2].name + " " + armies[2].loc), "C Paris")
        self.assertEqual((armies[3].name + " " + armies[3].loc), "D Prague")

    def test_eval_8(self):
        armies = [Army(["A", "Madrid", "Move", "London"]), Army(["B", "London", "Move", "Madrid"])]
        diplomacy_eval(armies)
        self.assertEqual((armies[0].name + " " + armies[0].loc), "A London")
        self.assertEqual((armies[1].name + " " + armies[1].loc), "B Madrid")


    # -----
    # print
    # -----

    def test_print_1(self):
        armies = [Army(["A", "Madrid", "Hold"])]
        w = StringIO()
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        armies = [Army(["A", "Madrid", "Hold"]), Army(["B", "London", "Hold"])]
        w = StringIO()
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "A Madrid\nB London\n")

    def test_print_3(self):
        armies = [Army(["A", "Madrid", "Hold"]), Army(["B", "London", "Hold"]), Army(["C", "Paris", "Hold"])]
        w = StringIO()
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "A Madrid\nB London\nC Paris\n")


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Move London\nB London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Madrid\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\nC Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC Paris\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\n\nB London Move Madrid\nC Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC Paris\n")



# ----
# main
# ----

if __name__ == "__main__": #pragma no cover
    main()
