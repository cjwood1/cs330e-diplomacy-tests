from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = 'B Chicago Move Boston'
        i = diplomacy_read(s)
        self.assertEqual(i[0],  'B')
        self.assertEqual(i[1], 'Chicago')
        self.assertEqual(i[2],  'Move')
        self.assertEqual(i[3],  'Boston')

    def test_read_2(self):
        s = 'A Madrid Hold'
        i = diplomacy_read(s)
        self.assertEqual(i[0],  'A')
        self.assertEqual(i[1], 'Madrid')
        self.assertEqual(i[2],  'Hold')

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold']])
        self.assertEqual(v, {'A': 'Madrid'})

    def test_eval_2(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B']])
        self.assertEqual(v, {'A': '[dead]', 'B': 'Madrid', 'C': 'London'})

    def test_eval_3(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid']])
        self.assertEqual(v, {'A': '[dead]', 'B': '[dead]'})

    def test_eval_4(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B'], ['D', 'Austin', 'Move', 'London']])
        self.assertEqual(v, {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': '[dead]'})

    def test_eval_5(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid']])
        self.assertEqual(v, {'A':'[dead]', 'B':'[dead]', 'C':'[dead]'})

    def test_eval_6(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'], ['D', 'Paris', 'Support', 'B']])
        self.assertEqual(v, {'A':'[dead]', 'B':'Madrid', 'C':'[dead]', 'D':'Paris'})

    def test_eval_7(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'], ['D', 'Paris', 'Support', 'B'], ['E', 'Austin', 'Support', 'A']])
        self.assertEqual(v, {'A':'[dead]', 'B':'[dead]', 'C':'[dead]', 'D':'Paris', 'E':'Austin'})
    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        diplomacy_print(w, {'A': 'NewYork'})
        self.assertEqual(w.getvalue(), 'A NewYork\n')

    def test_print2(self):
        w = StringIO()
        diplomacy_print(w, {'A': 'Madrid','B': '[dead]'})
        self.assertEqual(w.getvalue(), 'A Madrid\nB [dead]\n')
    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO('A NewYork Hold')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A NewYork\n')

    def test_solve2(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\n')

    def test_solve3(self):
        r = StringIO('A Chicago Move London\nB Boston Hold')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A London\nB Boston\n')

# ----
# main
# ----


if __name__ == '__main__': #pragma: no cover
    main()

''' #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
'''
